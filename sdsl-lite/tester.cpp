#include <sdsl/construct_sa_se.hpp>
//#include "construct_sa_se_modified.hpp"

#include <string>
#include <iostream>

using namespace sdsl;
using namespace std;

void print_sa(string f_name){
	int_vector_buffer<> sa(f_name);
	cout << '[';
	for(uint64_t i=0; i<sa.size(); ++i)
		if(i<sa.size()-1)
			cout << sa[i] << ' ';
		else
			cout << sa[i];
	cout  << ']' << endl;
}

int main() {
	
	cout << "Testprogramm für collection: (ABA,ABBA,BABA)\nText 1 = ABA $ ABBA $ BABA $ #\nText 2 = ABA $_1 ABBA $_2 BABA $_3 #\n\n";
	
	string text  = "232123321323210"; //string collection:  ABA $   ABBA $   BABA $   #
	string text2 = "454145542545430"; //string collection:  ABA $_1 ABBA $_2 BABA $_3 #
	
	//make string to int_vector:	
	int_vector<8> text_vec = int_vector<8>(text.length(),0,8);
	int_vector<8> text_vec2 = int_vector<8>(text.length(),0,8);
	for(int i = 0; i < text.length(); i++){
		text_vec[i]  = (int) text [i] - (int) '0';
		text_vec2[i] = (int) text2[i] - (int) '0';
	}
		
	//construct SA
	string f_name = "/tmp/sa_fname";
	string f_name2 = "/tmp/sa_fname2";
	
	_construct_sa_se(text_vec,f_name,256, 0);
	_construct_sa_se(text_vec2,f_name2,256, 0);
	

	
	//output SA:	
	cout << "SA of Text 1" << endl;
	print_sa(f_name);
	
	cout << "\nSA of Text 2" << endl;
	print_sa(f_name2);
	
	cout << "\nDein Ziel: Algorithmus so verändern, dass er für Text 1 das SA berechnet das momentan bei Text 2 rauskommt.\n\n";
	
	string f_name3 = "/tmp/sa_fname3";
	
	// hier kommst du ins Spiel die modified methode sollst du anlegen
	//_construct_sa_se_modified(text_vec,f_name3,256, 0); // has to match f_name2 (Text 2)
	
	
	cout << "modified SA of Text 1" << endl;
	//print_sa(f_name3);
}
