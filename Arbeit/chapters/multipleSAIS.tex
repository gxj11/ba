% !TeX spellcheck = en

\chapter{Induced suffix arrays for string collections}

The `Induced enhanced suffix array for string collection' was introduced by Louza, Gog and Telles in 2017 \cite{adjusted}. They used the existing SA-IS by Nong et al. and modified it to sort a collection of strings in one single suffix array. Since this altered algorithm is based on SA-IS, it also can be processed in $\mathcal{O}(n)$ time and work space what is going to be analysed in the next chapter.\\ % A possible use case would be sorting multiple documents.  
\\
As previously mentioned, the last position $S[n]$ of a string $S$ is defined as the sentinel $\$ = S[n]$. Since we have to handle $d$ strings, the sentinel $\$$ occurs multiple times, to be precise $d$ times because each string has exactly one sentinel. It not only serves as an indication of the end of a string, but also as a separator between different strings. Now, the sentinels are no more unique but they still have a lexicographic order what is needed to determine the order of the suffix array $SA$. The order is determined by the position in $S$, whereas any sentinel with a smaller index is lexicographically smaller than a sentinel with a greater index. In addition, a new lexicographically smallest character `$\#$' is appended at the very end of the entire concatenated string to indicate the end of the string collection.

\begin{definition}
	Let $T$ be a collection of $d$ nonempty strings of total length $N$, where $N = \sum_{i = 1}^{d}n_i + 1$ and $|S^i| = n_i$. Similar like before, the last character $T[N] = \#$ serves a special purpose. It is the lexicographically smallest character in $T$ and indicates the end of string $T$. To separate two strings $S^i$ and $S^j$ from each other, the sentinel $\$$ comes into use. The lexicographic order of all $\$$ characters is defined by $\$_i < \$_j,\ 1 < i < j < N$, where $i$ and $j$ are indexes in $T$. The new string $T$ is defined as follows: \[T = S^1[1..(n_1-1)] \cdot \$ \cdot S^2[1..(n_2-1)] \cdot \$\cdot \ ...\ \cdot S^d[1..(n_d-1)] \cdot \$ \cdot \# \]
\end{definition}

\begin{lemma}
	Every suffix $T_i$ with $T_i[1] = \$$ is S type and an LMS position except for $T_{N-1}$.
\end{lemma}
\begin{proof}
	That means that after every $T[i] = \$$ in $T$, except for $T[N-1] = \$$, there is a character $T[i+1] = c,\ c \in \Sigma\setminus\{\$,\#\}$, which is greater than $\$$, and that is correct because all strings are nonempty. Hence, almost every suffix starting with $\$$ is S type. To proof that $T[i] = \$$ is also an LMS position, the preceding character $T[i-1] = c$ has to be of L type by definition. Again, that is certainly correct since string are nonempty and like before $c$ is greater than $\$$. However, in one case that is wrong. The last suffix $T_{N-1}$ has to be L type even though it also starts with $\$$ because $T_N$ is lexicographically smaller than $T_{N-1}$, i.e. $T_{N-1} = \$\# > \# = T_N$. % 	Suppose all strings in $T$ are nonempty. 
\end{proof}

Given a string collection $T$, we want to construct a suffix array using the SA-IS algorithm. Although we have to treat string $T$ differently, the idea remains the same. Determine all types in phase 0, sort all LMS suffixes in phase 1 and sort all suffixes of $T$ in phase 2.\\
A rather obvious solution is to assign to every sentinel $\$$ in $T$ a unique character $\$_i$, where $\$_i < \$_j$ for $1 < i < j < N$, since the $\$$-characters have an order too. This way, the algorithm would not have to be modified and we could apply $T$ on the original algorithm. However, this undertaking demands an increased alphabet size $\sigma$ by $d$, given $d$ strings. To preserve the alphabet size, only one character ($\$$) is used after each individual string $S^i$, as described before, and a modification of the algorithm is needed.\\
For simplicity, a new array $dollar$ is introduced which stores the positions of all $\$$-characters of $T$ in an ascending order. We can easily calculate it by scanning $T$ from left to right and insert every occurrence of $\$$ in $dollar$. This procedure is necessary before phase 1 and in the top recursion level only. Please refer to algorithm \ref{dollar} for a detailed implementation.\\

\begin{algorithm}[H]
	\caption{The top recursion level (where $rec == 0$)  before phase 1. Let $dollar$ be the array to store all occurrences of $\$$ and $j$ be the current index that array.}\label{dollar}
	\begin{algorithmic}[1]    
		%if (rec == 0) for (int i = 0; i < n; ++i) if (text[i] == 1) dollar.push_back(i);
		\If {$rec == 0$} \Comment{The top recursion level}
		\State $j \gets 1$ \Comment{Index of the $dollar$ array}
		\For {$i \gets 1$ to $n$} \Comment{Scan $T$ from left ot right}
		\If {$T[i] == \$$} \Comment{The current position is a $\$$}
		\State $dollar[j] = i$ \Comment{Store index into $dollar$}
		\State increment $j$
		\EndIf
		\EndFor
		\EndIf
	\end{algorithmic}
\end{algorithm}

To sort a string collection, phase 1 and phase 2 have to be modified. Note that the following adjustment takes place at the top recursion level only, since the produced reduced string $\bar{T}$ does not rely on the original alphabet. If two equal LMS substrings are next to each other in $SA$, they will be sorted correctly in the recursion according to their occurrence in the newly generated string.\\ 
\\
The $\$$-bucket has to be treated differently because of the lexicographical arrangement of the $\$$-characters. As mentioned before, the suffix $T_{N-1}$ is L type. According to the original algorithm, the L type $\$$ is inserted at the beginning of the $\$$-bucket. This mistakenly means, $T_{N-1}$ is the smallest suffix of the $\$$-bucket, but in fact it is the greatest suffix starting with a $\$$ since it is the left most $\$$. To prevent this from happening, the $\$$-bucket is manually filled up in the correct order between step I and II in phase 1 and 2 on the top recursion level only.\\
In Step II of phase 1 and 2, all runs of L type suffixes are inserted into $SA$ and in step III of phase 1 and phase 2, all runs of S type suffixes are inserted into $SA$. Having in mind that almost every suffix starting with a $\$$ except for $T[N-1]$ is an LMS position, the $\$$-bucket will be overwritten because the LMS suffix right next to each $\$$ ends up overwriting the $\$$-bucket. To preserve the lexicographical order, again the $\$$-bucket is manually filled up in the correct order between step III and IV in phase 1 and 2 on the top recursion level only. Refer to algorithm \ref{mod} for an implementation.\\
A final change has to be made in the beginning of step II in both phases. Before step II, the $\$$-bucket in $SA$ has already been filled up correctly. According to the algorithm, $SA[1] - 1 = N-1$ will be inserted into $SA[2]$, since $T_{N-1} = \$\#$ is an L type suffix. However, that would change the $\$$-bucket. To circumvent that, $SA$ is scanned from $2$ to $N$. Certainly, $SA[2] \neq \bot$ since this is within the range of the previously filled $\$$-bucket. Also, $N-1$ has already been inserted into $SA$, to be precise at the end of the $\$$-bucket.  
\\
%Scan $T$ from right to left and check if the current suffix $T_i$ is an LMS position. If so, insert index $i$ at the current tail of its bucket $b$, where $b = T_{i}[1]$. A similar procedure is elaborated in phase 1, step I. When scanning through $T$, the following changes have to be made, in comparison with algorithm \ref{P1I}.\\
%When inserting the $\$$-characters into $SA$, the last or right most position of the $\$$-bucket, has to be reserved for suffix $T_{N-1}$ since it is the smallest suffix in the $\$$-bucket and the second smallest suffix of the entire string $T$.\\
%Another change is to not induce any suffix $T_i$ that starts with a $\$$-character, i.e. $T_i[1] = \$$. Therefore, while scanning $T$, if $T[i] = \$$, the previously scanned LMS suffix is being ignored despite being LMS position and the algorithm proceeds with the next entry.\\
%After the first scan, $T$ will be scanned once more from right to left inserting all $\$$ characters in into the $\$$-bucket according to their order. Refer to algorithm \ref{mult} for an implementation.

\begin{algorithm}[H]
	\caption{This change applies to the top recursion only. Using the $dollar$ array, the $\$$-bucket of the $SA$ array is filled up manually in the correct order.}\label{mod}
	\begin{algorithmic}[1]
		\If {$rec == 0$} \Comment{The top recursion level}
		\For {$i \gets 1$ to $|dollar|$}
		\State $SA[i+1] = i$ \Comment{Manually fill up the $\$$-bucket}
		\EndFor
		\EndIf
	\end{algorithmic}
\end{algorithm}

\begin{example}
	% TODO Beispiel
	Let {\ttfamily T = immisi\$misis\$isi\$\#} a collection of strings, meaning {\ttfamily S1 = immisi\$}, {\ttfamily S2 = misis\$} and {\ttfamily S3 = isi\$}.\\
	In the following figure 5, we apply those changes on the original algorithm using an example string $T$.\\
	% Tabelle wie in Phase 0
	% dann wie im Papier
	\begin{figure}[ht]
		\centering
		\label{mult_ex}
		%\vspace*{-0.8cm}  
		\hspace*{-1cm}  
		\includegraphics[width=1\textwidth]{images/altered}
	\end{figure}
\begin{figure}[ht]
	\hspace*{-1cm}  
	\includegraphics[width=1\textwidth]{images/altered2}
	\caption*{Figure 5:\\\hspace{\textwidth}a) Phase 0 and phase 1, step I: Types of all suffixes and the $P$ array are determined. Fields defined as $S^*$ denote an LMS.\\\hspace{\textwidth}b) Before we continue, the $dollar$ array is filled up with all positions where a $\$$-character occurs.\\\hspace{\textwidth}c) In phase 1, between step I and step II, the $\$$-bucket in $SA$ is filled up in order. In step II and III: All LMS, denoted by an $*$, are inserted into $SA$. After that, $SA$ is scanned from left to right, to insert all L types, and from right to left, to insert all S types. The $\$$-bucket is filled up in order once again.\\\hspace{\textwidth}d) Phase 1, step IV: The $LN$ is computed out of the $SA$ array.\\\hspace{\textwidth}e \& f) Phase 1, step V: Since every character in $\bar{S}$ occurs once only, $\bar{SA}$ is computed directly.\\\hspace{\textwidth}g) The order of LMS is determined by computing $res$ using $\bar{SA}$ and $P$.\\\hspace{\textwidth}h) Phase 2: Before step II and after step III, fill up the $\$$-bucket using the $dollar$ array.}
\end{figure}
\end{example}